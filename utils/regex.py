import re

wikidata_id_regex = re.compile('P\d+|Q\d+', re.I)

spinachbot_top_regex = re.compile('\{\{spinachbot top\}\}', re.I)
spinachbot_bottom_regex = re.compile('\{\{spinachbot bottom\}\}', re.I)