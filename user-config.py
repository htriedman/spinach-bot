import os
from dotenv import load_dotenv

log = ['*']

usernames['wikipedia']['*'] = \
  usernames['meta']['*'] = \
  usernames['commons']['*'] = \
  usernames['wikidata']['*'] = \
  usernames['wiktionary']['*'] = \
  usernames['wikibooks']['*'] = \
  usernames['wikinews']['*'] = \
  usernames['wikiquote']['*'] = \
  usernames['wikisource']['*'] = \
  usernames['wikiversity']['*'] = \
  usernames['wikivoyage']['*'] = \
  'SpinachBot'

load_dotenv()

authenticate['*.wikipedia.org'] = \
  authenticate['*.wikimedia.org'] = \
  authenticate['*.wikidata.org'] = \
  authenticate['*.wiktionary.org'] = \
  authenticate['*.wikibooks.org'] = \
  authenticate['*.wikinews.org'] = \
  authenticate['*.wikiquote.org'] = \
  authenticate['*.wikisource.org'] = \
  authenticate['*.wikiversity.org'] = \
  authenticate['*.wikivoyage.org'] = \
  authenticate['*.mediawiki.org'] = \
  (os.getenv('CONSUMER_TOKEN'), os.getenv('CONSUMER_SECRET'), os.getenv('ACCESS_TOKEN'), os.getenv('ACCESS_SECRET'))
